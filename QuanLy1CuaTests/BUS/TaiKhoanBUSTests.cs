﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuanLy1Cua.BUS;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.BUS.Tests
{
    [TestClass()]
    public class TaiKhoanBUSTests
    {
        [TestMethod()]
        public void CreateAccountTest()
        {
            TaiKhoan account = new TaiKhoan();
            account.TenDangNhap = "admin";
            account.MatKhauMD5 = "21232f297a57a5a743894a0e4a801fc3";
            account.LoaiTaiKhoanId = 1;
            account.NgayThem = DateTime.Now;
            account.NguoiThem = 1;
            account.TrangThai = false;

            TaiKhoanBUS bus = new TaiKhoanBUS();
            var action = bus.CreateAccount(account);
            Assert.IsNull(action);
        }

        [TestMethod()]
        public void GetAccountByCredentialsTest()
        {
            string username = "admin";
            string password = "21232f297a57a5a743894a0e4a801fc3";
            TaiKhoanBUS bus = new TaiKhoanBUS();
            var login = bus.GetAccountByCredentials(username, password);
            Assert.AreEqual("admin", login.TenDangNhap);
        }
    }
}