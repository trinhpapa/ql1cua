﻿using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.DATA.DAO
{
    public class LoaiGiayToDAO : BaseDAO<LoaiGiayTo, int>
    {
        private QL1CuaDbContext _ctx;

        public LoaiGiayToDAO()
        {
            _ctx = new QL1CuaDbContext();
        }

        public dynamic GetAlls()
        {
            return (from a in _ctx.LoaiGiayTo
                    join b in _ctx.DonVi on a.DonViTiepNhanId equals b.Id
                    select new
                    {
                        a.Id,
                        a.LoaiGiayTo1,
                        DonViTiepNhan = b.TenDonVi,
                        a.TrangThai
                    }).ToList();
        }
    }
}