﻿using QuanLy1Cua.DATA.DTO;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace QuanLy1Cua.DATA.DAO
{
    public class YeuCauXuLyDAO : BaseDAO<YeuCauXuLy, long>
    {
        private readonly QL1CuaDbContext _ctx;

        public YeuCauXuLyDAO()
        {
            _ctx = new QL1CuaDbContext();
        }

        public dynamic GetByStudentCode(string studentCode)
        {
            return _ctx.YeuCauXuLy.Where(i => i.MaSV == studentCode).FirstOrDefault();
        }

        public dynamic GetById(long id)
        {
            return (from a in _ctx.ChiTietYeuCauXuLy
                    join b in _ctx.YeuCauXuLy on a.YeuCauXuLyId equals b.Id into bs
                    from b in bs.DefaultIfEmpty()
                    join c in _ctx.LoaiGiayTo on a.LoaiGiayToId equals c.Id into cs
                    from c in cs.DefaultIfEmpty()
                    where a.Id == id
                    select new
                    {
                        b.HoTen,
                        b.MaSV,
                        b.DienThoai,
                        c.LoaiGiayTo1,
                        b.NgayTiepNhan,
                        a.NgayHenTra,
                    }).FirstOrDefault();
        }

        public dynamic SearchByFullName(string fullname)
        {
            return (from a in _ctx.ChiTietYeuCauXuLy
                    join b in _ctx.YeuCauXuLy on a.YeuCauXuLyId equals b.Id into bs
                    from b in bs.DefaultIfEmpty()
                    join c in _ctx.LoaiGiayTo on a.LoaiGiayToId equals c.Id into cs
                    from c in cs.DefaultIfEmpty()
                    join d in _ctx.DonVi on c.DonViTiepNhanId equals d.Id into ds
                    from d in ds.DefaultIfEmpty()
                    join e in _ctx.TrangThaiYeuCau on a.TrangThaiYeuCauId equals e.Id into es
                    from e in es.DefaultIfEmpty()
                    join f in _ctx.TaiKhoan on b.NguoiTiepNhan equals f.Id into fs
                    from f in fs.DefaultIfEmpty()
                    where b.HoTen.Contains(fullname) && a.TrangThai == true
                    select new
                    {
                        a.Id,
                        c.LoaiGiayTo1,
                        d.TenDonVi,
                        b.HoTen,
                        b.MaSV,
                        b.DienThoai,
                        b.NgayTiepNhan,
                        NguoiTiepNhan = f.HoTen ?? f.TenDangNhap,
                        e.TrangThaiYeuCau1,
                    }).ToList();
        }

        public dynamic GetAllByDay(DateTime date, int status)
        {
            if (status > 0)
            {
                return (from a in _ctx.ChiTietYeuCauXuLy
                        join b in _ctx.YeuCauXuLy on a.YeuCauXuLyId equals b.Id into bs
                        from b in bs.DefaultIfEmpty()
                        join c in _ctx.LoaiGiayTo on a.LoaiGiayToId equals c.Id into cs
                        from c in cs.DefaultIfEmpty()
                        join d in _ctx.DonVi on c.DonViTiepNhanId equals d.Id into ds
                        from d in ds.DefaultIfEmpty()
                        join e in _ctx.TrangThaiYeuCau on a.TrangThaiYeuCauId equals e.Id into es
                        from e in es.DefaultIfEmpty()
                        join f in _ctx.TaiKhoan on b.NguoiTiepNhan equals f.Id into fs
                        from f in fs.DefaultIfEmpty()
                        where b.NgayTiepNhan.Day == date.Day && b.NgayTiepNhan.Month == date.Month && b.NgayTiepNhan.Year == date.Year && a.TrangThaiYeuCauId == status && a.TrangThai == true
                        select new
                        {
                            a.Id,
                            c.LoaiGiayTo1,
                            d.TenDonVi,
                            b.HoTen,
                            b.MaSV,
                            b.DienThoai,
                            b.NgayTiepNhan,
                            NguoiTiepNhan = f.HoTen ?? f.TenDangNhap,
                            a.NgayHenTra,
                            e.TrangThaiYeuCau1,
                        }).OrderByDescending(e => e.NgayTiepNhan).ToList();
            }
            else
            {
                return (from a in _ctx.ChiTietYeuCauXuLy
                        join b in _ctx.YeuCauXuLy on a.YeuCauXuLyId equals b.Id into bs
                        from b in bs.DefaultIfEmpty()
                        join c in _ctx.LoaiGiayTo on a.LoaiGiayToId equals c.Id into cs
                        from c in cs.DefaultIfEmpty()
                        join d in _ctx.DonVi on c.DonViTiepNhanId equals d.Id into ds
                        from d in ds.DefaultIfEmpty()
                        join e in _ctx.TrangThaiYeuCau on a.TrangThaiYeuCauId equals e.Id into es
                        from e in es.DefaultIfEmpty()
                        join f in _ctx.TaiKhoan on b.NguoiTiepNhan equals f.Id into fs
                        from f in fs.DefaultIfEmpty()
                        where b.NgayTiepNhan.Day == date.Day && b.NgayTiepNhan.Month == date.Month && b.NgayTiepNhan.Year == date.Year && a.TrangThai == true
                        select new
                        {
                            a.Id,
                            c.LoaiGiayTo1,
                            d.TenDonVi,
                            b.HoTen,
                            b.MaSV,
                            b.DienThoai,
                            b.NgayTiepNhan,
                            a.NgayHenTra,
                            NguoiTiepNhan = f.HoTen ?? f.TenDangNhap,
                            e.TrangThaiYeuCau1,
                        }).OrderByDescending(e => e.NgayTiepNhan).ToList();
            }
        }
    }
}