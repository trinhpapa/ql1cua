﻿using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.DATA.DAO
{
    public class PhanQuyenLoaiTaiKhoanDAO : BaseDAO<PhanQuyenLoaiTaiKhoan, long>
    {
        private readonly QL1CuaDbContext _ctx;

        public PhanQuyenLoaiTaiKhoanDAO()
        {
            _ctx = new QL1CuaDbContext();
        }

        public List<int?> GetRoleByTypeAccount(int Id)
        {
            return _ctx.PhanQuyenLoaiTaiKhoan.Where(i => i.LoaiTaiKhoanId == Id).Select(i => i.QuyenId).ToList();
        }
    }
}