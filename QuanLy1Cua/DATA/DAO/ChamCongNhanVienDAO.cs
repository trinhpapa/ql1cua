﻿using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.DATA.DAO
{
    public class ChamCongNhanVienDAO : BaseDAO<ChamCongNhanVien, long>
    {
        private QL1CuaDbContext _ctx;

        public ChamCongNhanVienDAO()
        {
            _ctx = new QL1CuaDbContext();
        }

        public IEnumerable<ChamCongModel> GetListByMonth(DateTime month)
        {
            return (from a in _ctx.TaiKhoan
                    select new ChamCongModel
                    {
                        UserId = a.Id,
                        HoTen = a.HoTen,
                        TenDangNhap = a.TenDangNhap,
                        GioTrongThang = _ctx.ChamCongNhanVien.Where(i => i.UserId == a.Id && i.LogoutTime != null && i.LoginTime.Value.Month == month.Month && i.LoginTime.Value.Year == month.Year)
                        .Sum(i => DbFunctions.DiffMinutes(i.LoginTime, i.LogoutTime)).ToString(),
                    }).ToList();

            /*   SELECT b.HoTen, b.TenDangNhap, SUM(DATEDIFF(MINUTE,a.LoginTime, a.LogoutTime))
                 FROM ChamCongNhanVien a
                 INNER JOIN TaiKhoan b ON a.UserId = b.Id
                 WHERE b.Id = 1 AND a.LogoutTime IS NOT NULL
                 GROUP BY b.HoTen, b.TenDangNhap*/
        }

        public IEnumerable<ChamCongModel> GetListByUser(string tenDangNhap, DateTime time)
        {
            return (from a in _ctx.ChamCongNhanVien
                    join b in _ctx.TaiKhoan on a.UserId equals b.Id
                    where b.TenDangNhap == tenDangNhap && a.LoginTime.Value.Month == time.Month && a.LoginTime.Value.Year == time.Year
                    select new ChamCongModel
                    {
                        TenDangNhap = b.TenDangNhap,
                        ThoiGianDangNhap = a.LoginTime,
                        ThoiGianDangXuat = a.LogoutTime
                    }).ToList();
        }
    }
}