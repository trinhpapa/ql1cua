﻿using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QuanLy1Cua.DATA.DAO
{
    public abstract class BaseDAO<T, K> where T : class
    {
        private QL1CuaDbContext _ctx;

        protected BaseDAO()
        {
            _ctx = new QL1CuaDbContext();
        }

        public T Get(Func<T, bool> predicate)
        {
            return GetAll(predicate).FirstOrDefault();
        }

        public IEnumerable<T> GetAll(Func<T, bool> predicate = null)
        {
            IEnumerable<T> result = _ctx.Set<T>().AsEnumerable();
            return (predicate == null) ? result : result.Where<T>(predicate);
        }

        public void Add(T entity)
        {
            _ctx.Entry(entity).State = EntityState.Added;
        }

        public void Delete(Func<T, bool> predicate)
        {
            IEnumerable<T> entities = GetAll(predicate);
            foreach (T entity in entities)
            {
                _ctx.Entry(entity).State = EntityState.Deleted;
            }
        }

        public void Delete(T entity)
        {
            _ctx.Entry(entity).State = EntityState.Deleted;
        }

        public void Update(T entity)
        {
            _ctx.Entry(entity).State = EntityState.Modified;
        }

        public void Save()
        {
            _ctx.SaveChanges();
        }

        public void Dispose()
        {
            if (_ctx != null)
            {
                _ctx.Dispose();
            }
        }
    }
}