﻿using QuanLy1Cua.DATA.DTO;
using System.Collections.Generic;
using System.Linq;

namespace QuanLy1Cua.DATA.DAO
{
    public class TaiKhoanDAO : BaseDAO<TaiKhoan, long>
    {
        private readonly QL1CuaDbContext _ctx;

        public TaiKhoanDAO()
        {
            _ctx = new QL1CuaDbContext();
        }

        public dynamic GetAll()
        {
            return (from a in _ctx.TaiKhoan
                    join b in _ctx.LoaiTaiKhoan on a.LoaiTaiKhoanId equals b.Id
                    select new
                    {
                        a.Id,
                        a.HoTen,
                        a.TenDangNhap,
                        b.LoaiTaiKhoan1,
                        a.NgayThem,
                        a.NguoiThem,
                        a.TrangThai,
                    }).ToList();
        }
    }
}