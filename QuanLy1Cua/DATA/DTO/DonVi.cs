namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DonVi")]
    public partial class DonVi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DonVi()
        {
            LoaiGiayTo = new HashSet<LoaiGiayTo>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(1000)]
        public string TenDonVi { get; set; }

        [StringLength(50)]
        public string DienThoai { get; set; }

        public bool TrangThai { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoaiGiayTo> LoaiGiayTo { get; set; }
    }
}
