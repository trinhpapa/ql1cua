namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TrangThaiYeuCau")]
    public partial class TrangThaiYeuCau
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TrangThaiYeuCau()
        {
            ChiTietYeuCauXuLy = new HashSet<ChiTietYeuCauXuLy>();
        }

        public int Id { get; set; }

        [Column("TrangThaiYeuCau")]
        [Required]
        [StringLength(500)]
        public string TrangThaiYeuCau1 { get; set; }

        public bool TrangThai { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietYeuCauXuLy> ChiTietYeuCauXuLy { get; set; }
    }
}
