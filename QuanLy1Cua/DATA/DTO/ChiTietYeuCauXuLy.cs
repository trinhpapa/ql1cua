namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChiTietYeuCauXuLy")]
    public partial class ChiTietYeuCauXuLy
    {
        public long Id { get; set; }

        public long YeuCauXuLyId { get; set; }

        public int LoaiGiayToId { get; set; }

        public DateTime? NgayHenTra { get; set; }

        public int TrangThaiYeuCauId { get; set; }

        public long? NguoiDoiTrangThaiYeuCau { get; set; }

        public bool TrangThai { get; set; }

        public long? NguoiXoa { get; set; }

        public virtual LoaiGiayTo LoaiGiayTo { get; set; }

        public virtual TaiKhoan TaiKhoan { get; set; }

        public virtual TaiKhoan TaiKhoan1 { get; set; }

        public virtual TrangThaiYeuCau TrangThaiYeuCau { get; set; }

        public virtual YeuCauXuLy YeuCauXuLy { get; set; }
    }
}
