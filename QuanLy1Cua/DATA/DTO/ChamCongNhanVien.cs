namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChamCongNhanVien")]
    public partial class ChamCongNhanVien
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public DateTime? LoginTime { get; set; }

        public DateTime? LogoutTime { get; set; }
    }
}
