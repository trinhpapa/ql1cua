namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LoaiGiayTo")]
    public partial class LoaiGiayTo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LoaiGiayTo()
        {
            ChiTietYeuCauXuLy = new HashSet<ChiTietYeuCauXuLy>();
        }

        public int Id { get; set; }

        [Column("LoaiGiayTo")]
        [Required]
        [StringLength(1000)]
        public string LoaiGiayTo1 { get; set; }

        public int DonViTiepNhanId { get; set; }

        public bool TrangThai { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietYeuCauXuLy> ChiTietYeuCauXuLy { get; set; }

        public virtual DonVi DonVi { get; set; }
    }
}
