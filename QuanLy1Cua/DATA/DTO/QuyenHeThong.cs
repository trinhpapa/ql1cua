namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QuyenHeThong")]
    public partial class QuyenHeThong
    {
        public int Id { get; set; }

        [StringLength(500)]
        public string TenQuyen { get; set; }

        public short? Cap { get; set; }

        public int? QuyenChaId { get; set; }

        public bool? TrangThai { get; set; }
    }
}
