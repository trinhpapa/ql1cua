namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("YeuCauXuLy")]
    public partial class YeuCauXuLy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public YeuCauXuLy()
        {
            ChiTietYeuCauXuLy = new HashSet<ChiTietYeuCauXuLy>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(200)]
        public string HoTen { get; set; }

        [StringLength(50)]
        public string MaSV { get; set; }

        [StringLength(50)]
        public string DienThoai { get; set; }

        public DateTime NgayTiepNhan { get; set; }

        public long NguoiTiepNhan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietYeuCauXuLy> ChiTietYeuCauXuLy { get; set; }

        public virtual TaiKhoan TaiKhoan { get; set; }
    }
}
