namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class QL1CuaDbContext : DbContext
    {
        public QL1CuaDbContext()
            : base("name=QL1CuaDbContext")
        {
        }

        public virtual DbSet<ChamCongNhanVien> ChamCongNhanVien { get; set; }
        public virtual DbSet<ChiTietYeuCauXuLy> ChiTietYeuCauXuLy { get; set; }
        public virtual DbSet<DonVi> DonVi { get; set; }
        public virtual DbSet<LoaiGiayTo> LoaiGiayTo { get; set; }
        public virtual DbSet<LoaiTaiKhoan> LoaiTaiKhoan { get; set; }
        public virtual DbSet<PhanQuyenLoaiTaiKhoan> PhanQuyenLoaiTaiKhoan { get; set; }
        public virtual DbSet<QuyenHeThong> QuyenHeThong { get; set; }
        public virtual DbSet<TaiKhoan> TaiKhoan { get; set; }
        public virtual DbSet<TrangThaiYeuCau> TrangThaiYeuCau { get; set; }
        public virtual DbSet<YeuCauXuLy> YeuCauXuLy { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DonVi>()
                .HasMany(e => e.LoaiGiayTo)
                .WithRequired(e => e.DonVi)
                .HasForeignKey(e => e.DonViTiepNhanId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoaiGiayTo>()
                .HasMany(e => e.ChiTietYeuCauXuLy)
                .WithRequired(e => e.LoaiGiayTo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoaiTaiKhoan>()
                .HasMany(e => e.TaiKhoan)
                .WithRequired(e => e.LoaiTaiKhoan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TaiKhoan>()
                .HasMany(e => e.ChiTietYeuCauXuLy)
                .WithOptional(e => e.TaiKhoan)
                .HasForeignKey(e => e.NguoiDoiTrangThaiYeuCau);

            modelBuilder.Entity<TaiKhoan>()
                .HasMany(e => e.ChiTietYeuCauXuLy1)
                .WithOptional(e => e.TaiKhoan1)
                .HasForeignKey(e => e.NguoiXoa);

            modelBuilder.Entity<TaiKhoan>()
                .HasMany(e => e.TaiKhoan1)
                .WithRequired(e => e.TaiKhoan2)
                .HasForeignKey(e => e.NguoiThem);

            modelBuilder.Entity<TaiKhoan>()
                .HasMany(e => e.YeuCauXuLy)
                .WithRequired(e => e.TaiKhoan)
                .HasForeignKey(e => e.NguoiTiepNhan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TrangThaiYeuCau>()
                .HasMany(e => e.ChiTietYeuCauXuLy)
                .WithRequired(e => e.TrangThaiYeuCau)
                .WillCascadeOnDelete(false);
        }
    }
}