namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaiKhoan")]
    public partial class TaiKhoan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TaiKhoan()
        {
            ChiTietYeuCauXuLy = new HashSet<ChiTietYeuCauXuLy>();
            ChiTietYeuCauXuLy1 = new HashSet<ChiTietYeuCauXuLy>();
            TaiKhoan1 = new HashSet<TaiKhoan>();
            YeuCauXuLy = new HashSet<YeuCauXuLy>();
        }

        public long Id { get; set; }

        [StringLength(200)]
        public string HoTen { get; set; }

        [Required]
        [StringLength(100)]
        public string TenDangNhap { get; set; }

        [Required]
        [StringLength(500)]
        public string MatKhauMD5 { get; set; }

        public int LoaiTaiKhoanId { get; set; }

        public DateTime NgayThem { get; set; }

        public long NguoiThem { get; set; }

        public bool TrangThai { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietYeuCauXuLy> ChiTietYeuCauXuLy { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietYeuCauXuLy> ChiTietYeuCauXuLy1 { get; set; }

        public virtual LoaiTaiKhoan LoaiTaiKhoan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TaiKhoan> TaiKhoan1 { get; set; }

        public virtual TaiKhoan TaiKhoan2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<YeuCauXuLy> YeuCauXuLy { get; set; }
    }
}
