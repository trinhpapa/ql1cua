namespace QuanLy1Cua.DATA.DTO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhanQuyenLoaiTaiKhoan")]
    public partial class PhanQuyenLoaiTaiKhoan
    {
        public long Id { get; set; }

        public int? LoaiTaiKhoanId { get; set; }

        public int? QuyenId { get; set; }

        public long? NguoiThietLapId { get; set; }
    }
}