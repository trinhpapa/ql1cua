# Phần mềm quản lý bộ phận 1 cửa
**Mục đích:** 
```Xây dựng và quản lý tiếp nhận yêu cầu đơn bằng phần mềm, giảm thời gian làm việc cũng như dễ dàng quản lý hơn.```
## Hoạt động của chương trình

1. Form Login:
	- Tại form đăng nhập, người dùng nhập mật khẩu và tài khoản được cung cấp và nhấn nút "ĐĂNG NHẬP".
	- Nếu tài khoản, mật khẩu sai hoặc tài khoản bị khóa sẽ báo lỗi "Tài khoản hoặc mật khẩu không đúng".