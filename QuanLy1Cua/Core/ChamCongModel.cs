﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.Core
{
    public class ChamCongModel
    {
        public int STT { get; set; }
        public long UserId { get; set; }
        public string HoTen { get; set; }
        public string TenDangNhap { get; set; }
        public DateTime? ThoiGianDangNhap { get; set; }
        public DateTime? ThoiGianDangXuat { get; set; }
        public string GioTrongThang { get; set; }
    }
}