﻿using System;

namespace QuanLy1Cua.Core
{
    public class Constants
    {
        public static class UserInformation
        {
            public static long Id;

            public static string Username;

            public static string Fullname;

            public static DateTime LoginTime;

            public static DateTime LogoutTime;
        }

        public static class LoginForm
        {
            public static string TitleError = "Thông báo";

            public static string ContentError = "Tài khoản hoặc mật khẩu không đúng";
        }

        public static class HomeForm
        {
            public static string TitleLogout = "Thông báo";

            public static string ContentLogout = "Xác nhận thoát khỏi phần mềm?";
        }
    }
}