﻿using QuanLy1Cua.BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI
{
    public partial class frmTimekeeping : Form
    {
        private ChamCongNhanVienBUS _bus;

        public frmTimekeeping()
        {
            _bus = new ChamCongNhanVienBUS();
            InitializeComponent();
            dtpChooseMonth.CustomFormat = "MM/yyyy";
            dtpChooseMonth.ShowUpDown = true;
        }

        private void dgvUserList_Binding(DateTime time)
        {
            dgvUserList.AutoGenerateColumns = false;
            dgvUserList.DataSource = _bus.GetListByMonth(time);
        }

        private void dgvLoginTimeList_Binding(string username, DateTime time)
        {
            dgvLoginTimeList.AutoGenerateColumns = false;
            dgvLoginTimeList.DataSource = _bus.GetListByUser(username, time);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            dgvUserList_Binding(dtpChooseMonth.Value);
        }

        private void dgvUserList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            var username = dgvUserList.Rows[e.RowIndex].Cells[2].Value.ToString();
            dgvLoginTimeList_Binding(username, dtpChooseMonth.Value);
        }
    }
}