﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.Core;
using System;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI
{
    public partial class frmLogin : Form
    {
        private TaiKhoanBUS _tkBUS;
        private ChamCongNhanVienBUS _ccBUS;

        public frmLogin()
        {
            _tkBUS = new TaiKhoanBUS();
            _ccBUS = new ChamCongNhanVienBUS();
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var username = txtUsername.Text.Trim().ToLower();
            var passwordMD5 = StringHelper.ConverToMD5Hash(txtPassword.Text.Trim().ToLower());
            var checkAcount = _tkBUS.GetAccountByCredentials(username, passwordMD5);
            if (checkAcount != null)
            {
                Constants.UserInformation.Id = checkAcount.Id;
                Constants.UserInformation.Username = checkAcount.TenDangNhap;
                Constants.UserInformation.Fullname = checkAcount.HoTen;
                Constants.UserInformation.LoginTime = DateTime.Now;
                _ccBUS.LoginTime(Constants.UserInformation.Id, Constants.UserInformation.LoginTime);
                txtPassword.Clear();
                txtUsername.Focus();
                frmHome main = new frmHome();
                this.Hide();
                main.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show(Constants.LoginForm.ContentError, Constants.LoginForm.TitleError);
            }
        }

        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(sender, e);
                txtPassword.Clear();
                txtUsername.Focus();
            }
        }

        private void txtUsername_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (String.IsNullOrEmpty(txtPassword.Text.Trim()))
                {
                    txtPassword.Focus();
                }
                else
                {
                    btnLogin_Click(sender, e);
                }
            }
        }
    }
}