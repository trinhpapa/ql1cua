﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI
{
    public partial class frmUserManager : Form
    {
        private readonly TaiKhoanBUS _tkBUS;
        private readonly LoaiTaiKhoanBUS _ltkBUS;

        public frmUserManager()
        {
            InitializeComponent();

            _tkBUS = new TaiKhoanBUS();
            _ltkBUS = new LoaiTaiKhoanBUS();

            CheckRole();
            dgvData_Binding();
            cbbTypeAccount_Binding();
            cbbStatus_Binding();
        }

        private void CheckRole()
        {
            var data = _tkBUS.CheckRoleByUserId(Constants.UserInformation.Id);

            if (data.Contains(201))
            {
                btnAdd.Enabled = true;
            }
            if (data.Contains(202))
            {
                btnUpdate.Enabled = true;
            }
            if (data.Contains(203))
            {
                btnRemove.Enabled = true;
            }
        }

        public void dgvData_Binding()
        {
            dgvData.DataSource = _tkBUS.GetAll();
        }

        public void cbbTypeAccount_Binding()
        {
            cbbTypeAccount.DataSource = _ltkBUS.GetAllByStatus(true).ToList();
            cbbTypeAccount.ValueMember = "Id";
            cbbTypeAccount.DisplayMember = "LoaiTaiKhoan1";
        }

        public void cbbStatus_Binding()
        {
            var Item = new Dictionary<bool, string>();
            Item.Add(true, "Hoạt động");
            Item.Add(false, "Khóa");
            cbbStatus.DataSource = Item.ToList();
            cbbStatus.ValueMember = "Key";
            cbbStatus.DisplayMember = "Value";
        }

        private void dgvData_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            var Id = dgvData.Rows[e.RowIndex].Cells[0].Value;
            var data = _tkBUS.GetById((long)Id);
            txtFullname.Text = data.HoTen;
            txtUsername.Text = data.TenDangNhap;
            cbbTypeAccount.SelectedValue = data.LoaiTaiKhoanId;
            cbbStatus.SelectedValue = data.TrangThai;
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            var model = new TaiKhoan();
            model.HoTen = txtFullname.Text.Trim();
            model.TenDangNhap = txtUsername.Text.Trim();
            model.MatKhauMD5 = StringHelper.ConverToMD5Hash(txtPassword.Text.Trim());
            model.NgayThem = DateTime.Now;
            model.NguoiThem = Constants.UserInformation.Id;
            model.LoaiTaiKhoanId = (int)cbbTypeAccount.SelectedValue;
            model.TrangThai = (bool)cbbStatus.SelectedValue;
            var todo = _tkBUS.CreateAccount(model);
            if (todo == null)
            {
                MessageBox.Show("Thêm thất bại, kiểm tra lại tên đăng nhập");
            }
            else
            {
                dgvData_Binding();
                MessageBox.Show("Thêm thành công: " + todo);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var Id = dgvData.CurrentRow.Cells[0].Value;
            var todo = _tkBUS.RemoveAcount((long)Id);
            if (todo)
            {
                MessageBox.Show("Xóa thành công");
                dgvData_Binding();
            }
            else
            {
                MessageBox.Show("Xóa thất bại");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var model = new TaiKhoan();
            model.Id = (long)dgvData.CurrentRow.Cells[0].Value;
            model.HoTen = txtFullname.Text.Trim();
            model.TenDangNhap = txtUsername.Text.Trim();
            model.MatKhauMD5 = StringHelper.ConverToMD5Hash(txtPassword.Text.Trim());
            model.NguoiThem = Constants.UserInformation.Id;
            model.LoaiTaiKhoanId = (int)cbbTypeAccount.SelectedValue;
            model.TrangThai = (bool)cbbStatus.SelectedValue;
            var todo = _tkBUS.UpdateAccount(model);
            if (todo == null)
            {
                MessageBox.Show("Cập nhật thất bại, kiểm tra lại giá trị");
            }
            else
            {
                dgvData_Binding();
                MessageBox.Show("Cập nhật thành công");
            }
        }
    }
}