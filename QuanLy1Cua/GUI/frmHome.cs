﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DTO;
using QuanLy1Cua.GUI.FormAction;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI
{
    public partial class frmHome : Form
    {
        private TaiKhoanBUS _tkBUS;
        private YeuCauXuLyBUS _ycxlBUS;
        private TrangThaiYeuCauBUS _ttBUS;
        private ChamCongNhanVienBUS _ccBUS;

        public frmHome()
        {
            InitializeComponent();

            _tkBUS = new TaiKhoanBUS();
            _ycxlBUS = new YeuCauXuLyBUS();
            _ttBUS = new TrangThaiYeuCauBUS();
            _ccBUS = new ChamCongNhanVienBUS();

            LoadUserInformation();
            CheckRole();
            drdStatus_Load();
            dgvData_Load(DateTime.Now, -1);
        }

        private void CheckRole()
        {
            var data = _tkBUS.CheckRoleByUserId(Constants.UserInformation.Id);

            if (data.Contains(101))
            {
                btnAdd.Enabled = true;
            }
            if (data.Contains(102))
            {
                btnDelete.Enabled = true;
            }
            if (data.Contains(103))
            {
                btnChangeStatus.Enabled = true;
            }
            if (data.Contains(104))
            {
                btnExport.Enabled = true;
            }
            if (data.Contains(200))
            {
                btnUserManager.Enabled = true;
            }
            if (data.Contains(300))
            {
                btnRoleManager.Enabled = true;
            }
            if (data.Contains(400))
            {
                btnUnitManager.Enabled = true;
            }
            if (data.Contains(500))
            {
                btnLetterManager.Enabled = true;
            }
            if (data.Contains(700))
            {
                btnTimekeeping.Enabled = true;
            }
        }

        private void LoadUserInformation()
        {
            lbFullname.Text = Constants.UserInformation.Fullname ?? Constants.UserInformation.Username;
        }

        private void frmHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            var confirmResult = MessageBox.Show(Constants.HomeForm.ContentLogout, Constants.HomeForm.TitleLogout,
                                     MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirmResult == DialogResult.Yes)
            {
                _ccBUS.LogoutTime(Constants.UserInformation.Id, Constants.UserInformation.LoginTime, DateTime.Now);
            }
            else
            {
                e.Cancel = (confirmResult == DialogResult.No);
            }
        }

        private void dgvData_Load(DateTime date, int status)
        {
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = _ycxlBUS.GetAllByDate(date, status);
        }

        private void drdStatus_Load()
        {
            var data = _ttBUS.GetAll().ToList();
            data.Insert(0, new TrangThaiYeuCau { Id = -1, TrangThaiYeuCau1 = "Tất cả" });
            drdStatus.DataSource = data;
            drdStatus.ValueMember = "Id";
            drdStatus.DisplayMember = "TrangThaiYeuCau1";
        }

        private void llbLogout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        private void pnlUserManager_Click(object sender, EventArgs e)
        {
        }

        private void pnlQuanLyLoaiDon_Click(object sender, EventArgs e)
        {
            frmTypeLetterManager form = new frmTypeLetterManager();
            form.ShowDialog();
        }

        private void dtpFillDate_ValueChanged(object sender, EventArgs e)
        {
            dgvData_Load(dtpFillDate.Value, (int)drdStatus.SelectedValue);
        }

        private void btnFill_Click(object sender, EventArgs e)
        {
            dgvData_Load(dtpFillDate.Value, (int)drdStatus.SelectedValue);
        }

        private void txtSearchByName_TextChanged(object sender, EventArgs e)
        {
            var data = _ycxlBUS.SearchByFullname(txtSearchByName.Text.Trim());
            dgvData.DataSource = data;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmAdd frm = new frmAdd();
            frm.ShowDialog();
            dgvData_Load(dtpFillDate.Value, (int)drdStatus.SelectedValue);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                var confirmResult = MessageBox.Show("Xác nhận xóa yêu cầu?", "Thông báo",
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmResult == DialogResult.Yes)
                {
                    try
                    {
                        var delete = _ycxlBUS.ChangeStatusRemove((long)dgvData.Rows[dgvData.CurrentRow.Index].Cells[0].Value);
                        if (delete)
                        {
                            MessageBox.Show("Xóa thành công");
                        }
                        else
                        {
                            MessageBox.Show("Yêu cầu không thể xóa");
                        }
                        dgvData_Load(dtpFillDate.Value, (int)drdStatus.SelectedValue);
                    }
                    catch
                    {
                        MessageBox.Show("Đã xảy ra lỗi");
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn 1 bản ghi");
            }
        }

        private void btnChangeStatus_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                frmChangeStatus frm = new frmChangeStatus((long)dgvData.Rows[dgvData.CurrentRow.Index].Cells["Ma"].Value);
                frm.ShowDialog();
                dgvData_Load(dtpFillDate.Value, (int)drdStatus.SelectedValue);
            }
            else
            {
                MessageBox.Show("Vui lòng chọn 1 bản ghi");
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (dgvData.SelectedRows.Count > 0)
            {
                frmExport frm = new frmExport((long)dgvData.Rows[dgvData.CurrentRow.Index].Cells[0].Value);
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Vui lòng chọn 1 bản ghi");
            }
        }

        private void btnUserManager_Click(object sender, EventArgs e)
        {
            frmUserManager form = new frmUserManager();
            form.ShowDialog();
        }

        private void btnUnitManager_Click(object sender, EventArgs e)
        {
            frmUnitManager frm = new frmUnitManager();
            frm.ShowDialog();
        }

        private void btnLetterManager_Click(object sender, EventArgs e)
        {
            frmTypeLetterManager frm = new frmTypeLetterManager();
            frm.ShowDialog();
        }

        private void btnTimekeeping_Click(object sender, EventArgs e)
        {
            frmTimekeeping frm = new frmTimekeeping();
            frm.ShowDialog();
        }

        private void btnRoleManager_Click(object sender, EventArgs e)
        {
            frmRoleManager frm = new frmRoleManager();
            frm.ShowDialog();
            CheckRole();
        }

        private void dtpDateReply_ValueChanged(object sender, EventArgs e)
        {
        }
    }
}