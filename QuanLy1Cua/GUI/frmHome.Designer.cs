﻿namespace QuanLy1Cua.GUI
{
    partial class frmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHome));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRoleManager = new System.Windows.Forms.Button();
            this.btnTimekeeping = new System.Windows.Forms.Button();
            this.btnLetterManager = new System.Windows.Forms.Button();
            this.btnUnitManager = new System.Windows.Forms.Button();
            this.btnUserManager = new System.Windows.Forms.Button();
            this.pnlLine = new System.Windows.Forms.Panel();
            this.llbLogout = new System.Windows.Forms.LinkLabel();
            this.lbFullname = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlToolBox = new System.Windows.Forms.Panel();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnChangeStatus = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.Ma = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hoten = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Masinhvien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dienthoai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Loaidonxuly = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Donvitiepnhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ngaytiepnhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ngayhentra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nguoitiepnhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSearchByName = new System.Windows.Forms.TextBox();
            this.dtpFillDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDateReply = new System.Windows.Forms.DateTimePicker();
            this.btnFill = new System.Windows.Forms.Button();
            this.drdStatus = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlToolBox.SuspendLayout();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.btnRoleManager);
            this.panel1.Controls.Add(this.btnTimekeeping);
            this.panel1.Controls.Add(this.btnLetterManager);
            this.panel1.Controls.Add(this.btnUnitManager);
            this.panel1.Controls.Add(this.btnUserManager);
            this.panel1.Controls.Add(this.pnlLine);
            this.panel1.Controls.Add(this.llbLogout);
            this.panel1.Controls.Add(this.lbFullname);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 682);
            this.panel1.TabIndex = 0;
            // 
            // btnRoleManager
            // 
            this.btnRoleManager.BackColor = System.Drawing.Color.Azure;
            this.btnRoleManager.Enabled = false;
            this.btnRoleManager.FlatAppearance.BorderSize = 0;
            this.btnRoleManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRoleManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnRoleManager.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnRoleManager.Image = ((System.Drawing.Image)(resources.GetObject("btnRoleManager.Image")));
            this.btnRoleManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRoleManager.Location = new System.Drawing.Point(4, 225);
            this.btnRoleManager.Name = "btnRoleManager";
            this.btnRoleManager.Size = new System.Drawing.Size(190, 44);
            this.btnRoleManager.TabIndex = 12;
            this.btnRoleManager.Text = "  Quản lý nhóm tài khoản";
            this.btnRoleManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRoleManager.UseVisualStyleBackColor = false;
            this.btnRoleManager.Click += new System.EventHandler(this.btnRoleManager_Click);
            // 
            // btnTimekeeping
            // 
            this.btnTimekeeping.BackColor = System.Drawing.Color.Azure;
            this.btnTimekeeping.Enabled = false;
            this.btnTimekeeping.FlatAppearance.BorderSize = 0;
            this.btnTimekeeping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTimekeeping.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnTimekeeping.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnTimekeeping.Image = ((System.Drawing.Image)(resources.GetObject("btnTimekeeping.Image")));
            this.btnTimekeeping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTimekeeping.Location = new System.Drawing.Point(4, 372);
            this.btnTimekeeping.Name = "btnTimekeeping";
            this.btnTimekeeping.Size = new System.Drawing.Size(190, 44);
            this.btnTimekeeping.TabIndex = 11;
            this.btnTimekeeping.Text = "  Chấm công nhân viên";
            this.btnTimekeeping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTimekeeping.UseVisualStyleBackColor = false;
            this.btnTimekeeping.Click += new System.EventHandler(this.btnTimekeeping_Click);
            // 
            // btnLetterManager
            // 
            this.btnLetterManager.BackColor = System.Drawing.Color.Azure;
            this.btnLetterManager.Enabled = false;
            this.btnLetterManager.FlatAppearance.BorderSize = 0;
            this.btnLetterManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLetterManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLetterManager.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnLetterManager.Image = ((System.Drawing.Image)(resources.GetObject("btnLetterManager.Image")));
            this.btnLetterManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLetterManager.Location = new System.Drawing.Point(4, 323);
            this.btnLetterManager.Name = "btnLetterManager";
            this.btnLetterManager.Size = new System.Drawing.Size(190, 44);
            this.btnLetterManager.TabIndex = 9;
            this.btnLetterManager.Text = "  Quản lý loại giấy tờ";
            this.btnLetterManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLetterManager.UseVisualStyleBackColor = false;
            this.btnLetterManager.Click += new System.EventHandler(this.btnLetterManager_Click);
            // 
            // btnUnitManager
            // 
            this.btnUnitManager.BackColor = System.Drawing.Color.Azure;
            this.btnUnitManager.Enabled = false;
            this.btnUnitManager.FlatAppearance.BorderSize = 0;
            this.btnUnitManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnitManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnUnitManager.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnUnitManager.Image = ((System.Drawing.Image)(resources.GetObject("btnUnitManager.Image")));
            this.btnUnitManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUnitManager.Location = new System.Drawing.Point(4, 274);
            this.btnUnitManager.Name = "btnUnitManager";
            this.btnUnitManager.Size = new System.Drawing.Size(190, 44);
            this.btnUnitManager.TabIndex = 8;
            this.btnUnitManager.Text = "  Quản lý đơn vị";
            this.btnUnitManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUnitManager.UseVisualStyleBackColor = false;
            this.btnUnitManager.Click += new System.EventHandler(this.btnUnitManager_Click);
            // 
            // btnUserManager
            // 
            this.btnUserManager.BackColor = System.Drawing.Color.Azure;
            this.btnUserManager.Enabled = false;
            this.btnUserManager.FlatAppearance.BorderSize = 0;
            this.btnUserManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUserManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnUserManager.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnUserManager.Image = ((System.Drawing.Image)(resources.GetObject("btnUserManager.Image")));
            this.btnUserManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUserManager.Location = new System.Drawing.Point(4, 176);
            this.btnUserManager.Name = "btnUserManager";
            this.btnUserManager.Size = new System.Drawing.Size(190, 44);
            this.btnUserManager.TabIndex = 7;
            this.btnUserManager.Text = "  Quản lý tài khoản";
            this.btnUserManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUserManager.UseVisualStyleBackColor = false;
            this.btnUserManager.Click += new System.EventHandler(this.btnUserManager_Click);
            // 
            // pnlLine
            // 
            this.pnlLine.BackColor = System.Drawing.Color.White;
            this.pnlLine.Location = new System.Drawing.Point(0, 170);
            this.pnlLine.Name = "pnlLine";
            this.pnlLine.Size = new System.Drawing.Size(194, 1);
            this.pnlLine.TabIndex = 3;
            // 
            // llbLogout
            // 
            this.llbLogout.AutoSize = true;
            this.llbLogout.LinkColor = System.Drawing.Color.White;
            this.llbLogout.Location = new System.Drawing.Point(65, 148);
            this.llbLogout.Name = "llbLogout";
            this.llbLogout.Size = new System.Drawing.Size(56, 13);
            this.llbLogout.TabIndex = 2;
            this.llbLogout.TabStop = true;
            this.llbLogout.Text = "Đăng xuất";
            this.llbLogout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llbLogout_LinkClicked);
            // 
            // lbFullname
            // 
            this.lbFullname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbFullname.ForeColor = System.Drawing.Color.White;
            this.lbFullname.Location = new System.Drawing.Point(12, 127);
            this.lbFullname.Name = "lbFullname";
            this.lbFullname.Size = new System.Drawing.Size(166, 23);
            this.lbFullname.TabIndex = 1;
            this.lbFullname.Text = "Họ và tên";
            this.lbFullname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(39, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(110, 110);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pnlToolBox
            // 
            this.pnlToolBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlToolBox.BackColor = System.Drawing.Color.White;
            this.pnlToolBox.Controls.Add(this.btnExport);
            this.pnlToolBox.Controls.Add(this.btnChangeStatus);
            this.pnlToolBox.Controls.Add(this.btnDelete);
            this.pnlToolBox.Controls.Add(this.btnAdd);
            this.pnlToolBox.Location = new System.Drawing.Point(195, 2);
            this.pnlToolBox.Name = "pnlToolBox";
            this.pnlToolBox.Size = new System.Drawing.Size(1068, 40);
            this.pnlToolBox.TabIndex = 1;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.AliceBlue;
            this.btnExport.Enabled = false;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnExport.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnExport.Location = new System.Drawing.Point(392, 0);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(134, 40);
            this.btnExport.TabIndex = 8;
            this.btnExport.Text = " Xuất phiếu";
            this.btnExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnChangeStatus
            // 
            this.btnChangeStatus.BackColor = System.Drawing.Color.AliceBlue;
            this.btnChangeStatus.Enabled = false;
            this.btnChangeStatus.FlatAppearance.BorderSize = 0;
            this.btnChangeStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnChangeStatus.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnChangeStatus.Image = ((System.Drawing.Image)(resources.GetObject("btnChangeStatus.Image")));
            this.btnChangeStatus.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnChangeStatus.Location = new System.Drawing.Point(227, 0);
            this.btnChangeStatus.Name = "btnChangeStatus";
            this.btnChangeStatus.Size = new System.Drawing.Size(159, 40);
            this.btnChangeStatus.TabIndex = 7;
            this.btnChangeStatus.Text = " Đổi trạng thái";
            this.btnChangeStatus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnChangeStatus.UseVisualStyleBackColor = false;
            this.btnChangeStatus.Click += new System.EventHandler(this.btnChangeStatus_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.AliceBlue;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnDelete.ForeColor = System.Drawing.Color.OrangeRed;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnDelete.Location = new System.Drawing.Point(117, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(104, 40);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = " Xóa";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.AliceBlue;
            this.btnAdd.Enabled = false;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnAdd.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnAdd.Location = new System.Drawing.Point(7, 0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(104, 40);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = " Thêm";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMain.BackColor = System.Drawing.Color.White;
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.dgvData);
            this.pnlMain.Location = new System.Drawing.Point(195, 100);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1068, 577);
            this.pnlMain.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label6.Location = new System.Drawing.Point(2, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(165, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Danh sách yêu cầu xử lý:";
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ma,
            this.Hoten,
            this.Masinhvien,
            this.Dienthoai,
            this.Loaidonxuly,
            this.Donvitiepnhan,
            this.Ngaytiepnhan,
            this.Ngayhentra,
            this.Nguoitiepnhan,
            this.TrangThai});
            this.dgvData.Location = new System.Drawing.Point(4, 26);
            this.dgvData.MultiSelect = false;
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.Size = new System.Drawing.Size(1061, 543);
            this.dgvData.TabIndex = 0;
            // 
            // Ma
            // 
            this.Ma.DataPropertyName = "Id";
            this.Ma.FillWeight = 228.4264F;
            this.Ma.HeaderText = "Mã";
            this.Ma.Name = "Ma";
            this.Ma.ReadOnly = true;
            this.Ma.Width = 75;
            // 
            // Hoten
            // 
            this.Hoten.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Hoten.DataPropertyName = "Hoten";
            this.Hoten.FillWeight = 74.31472F;
            this.Hoten.HeaderText = "Họ tên";
            this.Hoten.Name = "Hoten";
            this.Hoten.ReadOnly = true;
            // 
            // Masinhvien
            // 
            this.Masinhvien.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Masinhvien.DataPropertyName = "MaSV";
            this.Masinhvien.FillWeight = 74.31472F;
            this.Masinhvien.HeaderText = "Mã sinh viên";
            this.Masinhvien.Name = "Masinhvien";
            this.Masinhvien.ReadOnly = true;
            // 
            // Dienthoai
            // 
            this.Dienthoai.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Dienthoai.DataPropertyName = "DienThoai";
            this.Dienthoai.FillWeight = 74.31472F;
            this.Dienthoai.HeaderText = "Điện thoại";
            this.Dienthoai.Name = "Dienthoai";
            this.Dienthoai.ReadOnly = true;
            // 
            // Loaidonxuly
            // 
            this.Loaidonxuly.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Loaidonxuly.DataPropertyName = "LoaiGiayTo1";
            this.Loaidonxuly.HeaderText = "Loại đơn xử lý";
            this.Loaidonxuly.Name = "Loaidonxuly";
            this.Loaidonxuly.ReadOnly = true;
            // 
            // Donvitiepnhan
            // 
            this.Donvitiepnhan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Donvitiepnhan.DataPropertyName = "TenDonVi";
            this.Donvitiepnhan.HeaderText = "Đơn vị tiếp nhận";
            this.Donvitiepnhan.Name = "Donvitiepnhan";
            this.Donvitiepnhan.ReadOnly = true;
            // 
            // Ngaytiepnhan
            // 
            this.Ngaytiepnhan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Ngaytiepnhan.DataPropertyName = "NgayTiepNhan";
            this.Ngaytiepnhan.FillWeight = 74.31472F;
            this.Ngaytiepnhan.HeaderText = "Ngày tiếp nhận";
            this.Ngaytiepnhan.Name = "Ngaytiepnhan";
            this.Ngaytiepnhan.ReadOnly = true;
            // 
            // Ngayhentra
            // 
            this.Ngayhentra.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Ngayhentra.DataPropertyName = "NgayHenTra";
            this.Ngayhentra.HeaderText = "Ngày hẹn trả";
            this.Ngayhentra.Name = "Ngayhentra";
            this.Ngayhentra.ReadOnly = true;
            // 
            // Nguoitiepnhan
            // 
            this.Nguoitiepnhan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nguoitiepnhan.DataPropertyName = "NguoiTiepNhan";
            this.Nguoitiepnhan.FillWeight = 74.31472F;
            this.Nguoitiepnhan.HeaderText = "Người tiếp nhận";
            this.Nguoitiepnhan.Name = "Nguoitiepnhan";
            this.Nguoitiepnhan.ReadOnly = true;
            // 
            // TrangThai
            // 
            this.TrangThai.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TrangThai.DataPropertyName = "TrangThaiYeuCau1";
            this.TrangThai.HeaderText = "Trạng Thái";
            this.TrangThai.Name = "TrangThai";
            this.TrangThai.ReadOnly = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(2, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tìm kiếm tên:";
            // 
            // txtSearchByName
            // 
            this.txtSearchByName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchByName.Location = new System.Drawing.Point(85, 15);
            this.txtSearchByName.Name = "txtSearchByName";
            this.txtSearchByName.Size = new System.Drawing.Size(146, 20);
            this.txtSearchByName.TabIndex = 1;
            this.txtSearchByName.TextChanged += new System.EventHandler(this.txtSearchByName_TextChanged);
            // 
            // dtpFillDate
            // 
            this.dtpFillDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFillDate.Location = new System.Drawing.Point(329, 15);
            this.dtpFillDate.Name = "dtpFillDate";
            this.dtpFillDate.Size = new System.Drawing.Size(142, 20);
            this.dtpFillDate.TabIndex = 4;
            this.dtpFillDate.ValueChanged += new System.EventHandler(this.dtpFillDate_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label7.Location = new System.Drawing.Point(237, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "Lọc ngày thêm:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.dtpDateReply);
            this.panel2.Controls.Add(this.btnFill);
            this.panel2.Controls.Add(this.drdStatus);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtSearchByName);
            this.panel2.Controls.Add(this.dtpFillDate);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(195, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1068, 48);
            this.panel2.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(477, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Lọc ngày hẹn trả:";
            // 
            // dtpDateReply
            // 
            this.dtpDateReply.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDateReply.Location = new System.Drawing.Point(583, 15);
            this.dtpDateReply.Name = "dtpDateReply";
            this.dtpDateReply.Size = new System.Drawing.Size(142, 20);
            this.dtpDateReply.TabIndex = 9;
            this.dtpDateReply.ValueChanged += new System.EventHandler(this.dtpDateReply_ValueChanged);
            // 
            // btnFill
            // 
            this.btnFill.BackColor = System.Drawing.Color.LimeGreen;
            this.btnFill.FlatAppearance.BorderSize = 0;
            this.btnFill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFill.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnFill.ForeColor = System.Drawing.Color.White;
            this.btnFill.Location = new System.Drawing.Point(961, 11);
            this.btnFill.Name = "btnFill";
            this.btnFill.Size = new System.Drawing.Size(98, 23);
            this.btnFill.TabIndex = 8;
            this.btnFill.Text = "Lọc trạng thái";
            this.btnFill.UseVisualStyleBackColor = false;
            this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
            // 
            // drdStatus
            // 
            this.drdStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drdStatus.FormattingEnabled = true;
            this.drdStatus.Location = new System.Drawing.Point(795, 13);
            this.drdStatus.Name = "drdStatus";
            this.drdStatus.Size = new System.Drawing.Size(158, 21);
            this.drdStatus.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(731, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Trạng thái:";
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlToolBox);
            this.Controls.Add(this.panel1);
            this.Name = "frmHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần Mềm Quản Lý Một Cửa - Đại Học Vinh";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHome_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlToolBox.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbFullname;
        private System.Windows.Forms.LinkLabel llbLogout;
        private System.Windows.Forms.Panel pnlLine;
        private System.Windows.Forms.Panel pnlToolBox;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSearchByName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpFillDate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox drdStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnFill;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ma;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hoten;
        private System.Windows.Forms.DataGridViewTextBoxColumn Masinhvien;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dienthoai;
        private System.Windows.Forms.DataGridViewTextBoxColumn Loaidonxuly;
        private System.Windows.Forms.DataGridViewTextBoxColumn Donvitiepnhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ngaytiepnhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ngayhentra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nguoitiepnhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai;
        private System.Windows.Forms.Button btnUserManager;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnChangeStatus;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnUnitManager;
        private System.Windows.Forms.Button btnLetterManager;
        private System.Windows.Forms.Button btnTimekeeping;
        private System.Windows.Forms.Button btnRoleManager;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDateReply;
    }
}