﻿using QuanLy1Cua.BUS;
using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI.FormAction
{
    public partial class frmExport : Form
    {
        private YeuCauXuLyBUS _ycxlBUS;
        private string _HoTen;
        private string _MSV;
        private string _SDT;
        private string _TenGiayTo;
        private DateTime _NgayTiepNhan;
        private DateTime _NgayHenTra;

        public frmExport(long Id)
        {
            InitializeComponent();
            _ycxlBUS = new YeuCauXuLyBUS();
            GetInformation(Id);
            lbHoTen.Text = _HoTen;
            lbMSV.Text = _MSV;
            lbDienThoai.Text = _SDT;
            lbLoaiGiayTo.Text = _TenGiayTo;
            lbNgayTiepNhan.Text = _NgayTiepNhan.ToString("dd/MM/yyyy");
            lbNgayHenTra.Text = _NgayHenTra.ToString("dd/MM/yyyy");
        }

        private void GetInformation(long _Id)
        {
            var item = _ycxlBUS.GetById(_Id);
            _HoTen = item.HoTen;
            _MSV = item.MaSV;
            _SDT = item.DienThoai;
            _TenGiayTo = item.LoaiGiayTo1;
            _NgayTiepNhan = item.NgayTiepNhan;
            _NgayHenTra = item.NgayHenTra;
        }

        private void CreateReport(object sender, PrintPageEventArgs e)
        {
            Graphics grp = e.Graphics;
            Rectangle rect;
            Font font = new Font("Courier New", 12);
            float fontHeight = font.GetHeight();

            StringFormat center = new StringFormat();
            center.LineAlignment = StringAlignment.Center;
            center.Alignment = StringAlignment.Center;

            int offSet = 0;

            rect = new Rectangle(0, 0, 750, (int)fontHeight);
            grp.DrawString("TRƯỜNG ĐẠI HỌC VINH - VINH UNIVERSITY", font, new SolidBrush(Color.Black), rect, center);

            offSet += (int)fontHeight;
            rect = new Rectangle(0, offSet, 750, (int)fontHeight);
            grp.DrawString("BỘ PHẬN TIẾP NHẬN MỘT CỬA", font, new SolidBrush(Color.Black), rect, center);

            offSet += (int)fontHeight;
            rect = new Rectangle(0, offSet, 750, (int)fontHeight);
            grp.DrawString("__o0o__", font, new SolidBrush(Color.Black), rect, center);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(0, offSet + 20, 750, (int)fontHeight);
            grp.DrawString("PHIẾU HẸN KẾT QUẢ", new Font("Courier New", 14, FontStyle.Bold), new SolidBrush(Color.Black), rect, center);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 50, 750, (int)fontHeight);
            grp.DrawString($"Họ và tên: {_HoTen}", font, new SolidBrush(Color.Black), rect);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 55, 750, (int)fontHeight);
            grp.DrawString($"Mã sinh viên: {_MSV}", font, new SolidBrush(Color.Black), rect);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 60, 750, (int)fontHeight);
            grp.DrawString($"Điện thoại: {_SDT}", font, new SolidBrush(Color.Black), rect);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 65, 750, (int)fontHeight);
            grp.DrawString($"Giấy tờ xử lý: {_TenGiayTo}", font, new SolidBrush(Color.Black), rect);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 70, 750, (int)fontHeight);
            grp.DrawString($"Ngày tiếp nhận: {_NgayTiepNhan.ToString("dd/MM/yyyy")}", font, new SolidBrush(Color.Black), rect);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 75, 750, (int)fontHeight);
            grp.DrawString($"Ngày hẹn trả: {_NgayHenTra.ToString("dd/MM/yyyy")}", font, new SolidBrush(Color.Black), rect);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 80, 750, (int)fontHeight);
            grp.DrawString("--------------------------------", font, new SolidBrush(Color.Black), rect);

            offSet = offSet + (int)fontHeight;
            rect = new Rectangle(150, offSet + 82, 750, (int)fontHeight);
            grp.DrawString("(*) Khi nhận kết quả mang theo phiếu này", new Font("Courier New", 8), new SolidBrush(Color.Black), rect);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrintDialog _PrintDialog = new PrintDialog();
                PrintDocument _PrintDocument = new PrintDocument();

                _PrintDialog.Document = _PrintDocument;

                _PrintDocument.PrintPage += new PrintPageEventHandler(CreateReport);

                DialogResult result = _PrintDialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    _PrintDocument.Print();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}