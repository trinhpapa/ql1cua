﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI.FormAction
{
    public partial class frmAdd : Form
    {
        private LoaiGiayToBUS _lgtBUS;

        private YeuCauXuLyBUS _ycxlBUS;

        public frmAdd()
        {
            InitializeComponent();

            _lgtBUS = new LoaiGiayToBUS();
            _ycxlBUS = new YeuCauXuLyBUS();

            drdDonVi_Load();
        }

        public void drdDonVi_Load()
        {
            drdTypeLetter.DataSource = _lgtBUS.GetAll();
            drdTypeLetter.ValueMember = "Id";
            drdTypeLetter.DisplayMember = "LoaiGiayTo1";
        }

        private void btnAddLetter_Click(object sender, System.EventArgs e)
        {
            int id = (int)drdTypeLetter.SelectedValue;
            var item = _lgtBUS.GetInfo(id);
            bool check = false;
            if (dgvLetter.Rows.Count > 0)
            {
                for (int i = 0; i < dgvLetter.Rows.Count; i++)
                {
                    if ((int)dgvLetter.Rows[i].Cells[0].Value == item.Id)
                    {
                        check = false;
                    }
                    else
                    {
                        check = true;
                    }
                }
                if (check == false)
                {
                    MessageBox.Show("Đã tồn tại");
                }
                else
                {
                    dgvLetter.Rows.Add(item.Id, item.LoaiGiayTo1, item.DonVi.TenDonVi, dtpNgayHenTra.Value);
                }
            }
            else
            {
                dgvLetter.Rows.Add(item.Id, item.LoaiGiayTo1, item.DonVi.TenDonVi, dtpNgayHenTra.Value);
            }
        }

        private void btnDeleteLetter_Click(object sender, System.EventArgs e)
        {
            if (dgvLetter.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow item in dgvLetter.SelectedRows)
                {
                    dgvLetter.Rows.RemoveAt(item.Index);
                }
            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            txtFullname.Clear();
            txtFullname.Focus();
            txtStudentCode.Clear();
            txtPhoneNumber.Clear();
            dgvLetter.Rows.Clear();
        }

        public void CheckValidTextBox(TextBox input)
        {
            if (input.Text.Trim() == string.Empty)
            {
                input.Focus();
                return;
            }
        }

        public dynamic CheckStudentCodeExist(TextBox input)
        {
            return _ycxlBUS.CheckStudentCodeExist(input.Text.Trim());
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            CheckValidTextBox(txtFullname);
            CheckValidTextBox(txtStudentCode);
            CheckValidTextBox(txtPhoneNumber);
            if (dgvLetter.Rows.Count < 1)
            {
                MessageBox.Show("Phải chọn ít nhất 1 loại giấy tờ");
                return;
            }
            var studentCodeIfExist = CheckStudentCodeExist(txtStudentCode);

            if (studentCodeIfExist != null)
            {
                if (txtFullname.Text != studentCodeIfExist.HoTen)
                {
                    MessageBox.Show("Mã sinh viên đã có, lấy thông tin từ hệ thống");
                    txtFullname.Text = studentCodeIfExist.HoTen;
                    txtPhoneNumber.Text = studentCodeIfExist.DienThoai;
                    return;
                }
            }

            List<ItemYCXL> letterItems = new List<ItemYCXL>();
            foreach (DataGridViewRow row in dgvLetter.Rows)
            {
                var item = new ItemYCXL();

                item.Id = (int)row.Cells[0].Value;
                item.NgayHen = (DateTime)row.Cells[3].Value;
                letterItems.Add(item);
            }

            _ycxlBUS.Add(txtFullname.Text.Trim(), txtStudentCode.Text.Trim(), txtPhoneNumber.Text.Trim(), Constants.UserInformation.Id, letterItems);

            btnCancel_Click(sender, e);

            MessageBox.Show("Thêm yêu cầu thành công");
        }
    }
}