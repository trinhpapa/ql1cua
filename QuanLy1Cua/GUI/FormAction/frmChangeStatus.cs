﻿using QuanLy1Cua.BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI.FormAction
{
    public partial class frmChangeStatus : Form
    {
        private long _Id;

        private YeuCauXuLyBUS _ycxlBUS;

        public frmChangeStatus(long Id)
        {
            InitializeComponent();
            _Id = Id;
            _ycxlBUS = new YeuCauXuLyBUS();
        }

        private void UpdateStatus(int status)
        {
            var confirmResult = MessageBox.Show("Xác nhận đổi trạng thái", "Thông báo",
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirmResult == DialogResult.Yes)
            {
                try
                {
                    _ycxlBUS.ChangeStatus(_Id, status);
                    MessageBox.Show("Sửa thành công");
                    this.Close();
                }
                catch
                {
                    MessageBox.Show("Đã xảy ra lỗi");
                }
            }
            else
            {
                return;
            }
        }

        private void btnStatusOne_Click(object sender, EventArgs e)
        {
            UpdateStatus(1);
        }

        private void btnStatusTwo_Click(object sender, EventArgs e)
        {
            UpdateStatus(2);
        }

        private void btnStatusThree_Click(object sender, EventArgs e)
        {
            UpdateStatus(3);
        }

        private void btnStatusFour_Click(object sender, EventArgs e)
        {
            UpdateStatus(4);
        }
    }
}