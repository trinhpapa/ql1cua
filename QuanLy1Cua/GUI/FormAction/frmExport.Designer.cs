﻿namespace QuanLy1Cua.GUI.FormAction
{
    partial class frmExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExport));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbHoTen = new System.Windows.Forms.Label();
            this.lbMSV = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbDienThoai = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbLoaiGiayTo = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbNgayTiepNhan = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbNgayHenTra = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(1, 348);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(349, 48);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "TRƯỜNG ĐẠI HỌC VINH - VINH UNIVERSITY";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "BỘ PHẬN QUẢN LÝ MỘT CỬA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(145, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "__o0o__";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(92, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "PHIẾU HẸN KẾT QUẢ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Họ và tên:";
            // 
            // lbHoTen
            // 
            this.lbHoTen.AutoSize = true;
            this.lbHoTen.Location = new System.Drawing.Point(134, 143);
            this.lbHoTen.Name = "lbHoTen";
            this.lbHoTen.Size = new System.Drawing.Size(35, 13);
            this.lbHoTen.TabIndex = 7;
            this.lbHoTen.Text = "label6";
            // 
            // lbMSV
            // 
            this.lbMSV.AutoSize = true;
            this.lbMSV.Location = new System.Drawing.Point(134, 170);
            this.lbMSV.Name = "lbMSV";
            this.lbMSV.Size = new System.Drawing.Size(35, 13);
            this.lbMSV.TabIndex = 9;
            this.lbMSV.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(45, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Mã sinh viên:";
            // 
            // lbDienThoai
            // 
            this.lbDienThoai.AutoSize = true;
            this.lbDienThoai.Location = new System.Drawing.Point(134, 199);
            this.lbDienThoai.Name = "lbDienThoai";
            this.lbDienThoai.Size = new System.Drawing.Size(35, 13);
            this.lbDienThoai.TabIndex = 11;
            this.lbDienThoai.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(45, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Điện thoại";
            // 
            // lbLoaiGiayTo
            // 
            this.lbLoaiGiayTo.AutoSize = true;
            this.lbLoaiGiayTo.Location = new System.Drawing.Point(134, 228);
            this.lbLoaiGiayTo.Name = "lbLoaiGiayTo";
            this.lbLoaiGiayTo.Size = new System.Drawing.Size(41, 13);
            this.lbLoaiGiayTo.TabIndex = 13;
            this.lbLoaiGiayTo.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(45, 228);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Giấy tờ xử lý";
            // 
            // lbNgayTiepNhan
            // 
            this.lbNgayTiepNhan.AutoSize = true;
            this.lbNgayTiepNhan.Location = new System.Drawing.Point(134, 256);
            this.lbNgayTiepNhan.Name = "lbNgayTiepNhan";
            this.lbNgayTiepNhan.Size = new System.Drawing.Size(41, 13);
            this.lbNgayTiepNhan.TabIndex = 15;
            this.lbNgayTiepNhan.Text = "label13";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(45, 256);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Ngày tiếp nhận:";
            // 
            // lbNgayHenTra
            // 
            this.lbNgayHenTra.AutoSize = true;
            this.lbNgayHenTra.Location = new System.Drawing.Point(134, 285);
            this.lbNgayHenTra.Name = "lbNgayHenTra";
            this.lbNgayHenTra.Size = new System.Drawing.Size(41, 13);
            this.lbNgayHenTra.TabIndex = 17;
            this.lbNgayHenTra.Text = "label15";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(45, 285);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Ngày hẹn trả:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button1.Location = new System.Drawing.Point(277, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 40);
            this.button1.TabIndex = 2;
            this.button1.Text = " In";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(350, 395);
            this.Controls.Add(this.lbNgayHenTra);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lbNgayTiepNhan);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lbLoaiGiayTo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lbDienThoai);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbMSV);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbHoTen);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmExport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xuất phiếu yêu cầu";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbHoTen;
        private System.Windows.Forms.Label lbMSV;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbDienThoai;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbLoaiGiayTo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbNgayTiepNhan;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbNgayHenTra;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button1;
    }
}