﻿namespace QuanLy1Cua.GUI.FormAction
{
    partial class frmChangeStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnStatusOne = new System.Windows.Forms.Button();
            this.btnStatusTwo = new System.Windows.Forms.Button();
            this.btnStatusThree = new System.Windows.Forms.Button();
            this.btnStatusFour = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnStatusFour);
            this.groupBox1.Controls.Add(this.btnStatusThree);
            this.groupBox1.Controls.Add(this.btnStatusTwo);
            this.groupBox1.Controls.Add(this.btnStatusOne);
            this.groupBox1.Location = new System.Drawing.Point(2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(606, 84);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thay đổi trạng thái yêu cầu xử lý";
            // 
            // btnStatusOne
            // 
            this.btnStatusOne.BackColor = System.Drawing.Color.Gold;
            this.btnStatusOne.Location = new System.Drawing.Point(10, 31);
            this.btnStatusOne.Name = "btnStatusOne";
            this.btnStatusOne.Size = new System.Drawing.Size(138, 38);
            this.btnStatusOne.TabIndex = 0;
            this.btnStatusOne.Text = "Đã tiếp nhận";
            this.btnStatusOne.UseVisualStyleBackColor = false;
            this.btnStatusOne.Click += new System.EventHandler(this.btnStatusOne_Click);
            // 
            // btnStatusTwo
            // 
            this.btnStatusTwo.BackColor = System.Drawing.Color.Red;
            this.btnStatusTwo.Location = new System.Drawing.Point(154, 31);
            this.btnStatusTwo.Name = "btnStatusTwo";
            this.btnStatusTwo.Size = new System.Drawing.Size(142, 38);
            this.btnStatusTwo.TabIndex = 1;
            this.btnStatusTwo.Text = "Đã gửi yêu cầu xử lý";
            this.btnStatusTwo.UseVisualStyleBackColor = false;
            this.btnStatusTwo.Click += new System.EventHandler(this.btnStatusTwo_Click);
            // 
            // btnStatusThree
            // 
            this.btnStatusThree.BackColor = System.Drawing.Color.LimeGreen;
            this.btnStatusThree.Location = new System.Drawing.Point(302, 31);
            this.btnStatusThree.Name = "btnStatusThree";
            this.btnStatusThree.Size = new System.Drawing.Size(144, 38);
            this.btnStatusThree.TabIndex = 2;
            this.btnStatusThree.Text = "Đã xử lý";
            this.btnStatusThree.UseVisualStyleBackColor = false;
            this.btnStatusThree.Click += new System.EventHandler(this.btnStatusThree_Click);
            // 
            // btnStatusFour
            // 
            this.btnStatusFour.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnStatusFour.Location = new System.Drawing.Point(452, 31);
            this.btnStatusFour.Name = "btnStatusFour";
            this.btnStatusFour.Size = new System.Drawing.Size(144, 38);
            this.btnStatusFour.TabIndex = 3;
            this.btnStatusFour.Text = "Đã trả yêu cầu";
            this.btnStatusFour.UseVisualStyleBackColor = false;
            this.btnStatusFour.Click += new System.EventHandler(this.btnStatusFour_Click);
            // 
            // frmChangeStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 90);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmChangeStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản lý Yêu cầu xử lý";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnStatusThree;
        private System.Windows.Forms.Button btnStatusTwo;
        private System.Windows.Forms.Button btnStatusOne;
        private System.Windows.Forms.Button btnStatusFour;
    }
}