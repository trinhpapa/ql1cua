﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI.FormAction
{
    public partial class frmEditRole : Form
    {
        private LoaiTaiKhoanBUS _ltkBUS;
        private int? typeAccountId { get; set; }

        public frmEditRole(int? _typeAccount) : base()
        {
            typeAccountId = _typeAccount;
            InitializeComponent();
            _ltkBUS = new LoaiTaiKhoanBUS();
        }

        private void frmEditRole_Load(object sender, EventArgs e)
        {
            if (typeAccountId != null)
            {
                var data = _ltkBUS.LoaiTaiKhoan_GetById((int)typeAccountId);
                txtName.Text = data.LoaiTaiKhoan1;
                if (data.TrangThai)
                {
                    chkStatus.Checked = true;
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                if (typeAccountId == null)
                {
                    var item = new LoaiTaiKhoan();
                    item.LoaiTaiKhoan1 = txtName.Text;
                    item.TrangThai = false;
                    if (chkStatus.Checked)
                        item.TrangThai = true;
                    var todo = _ltkBUS.LoaiTaiKhoan_Create(item);
                    if (todo)
                    {
                        this.Close();
                    }
                }
                else
                {
                    var item = new LoaiTaiKhoan();
                    item.Id = (int)typeAccountId;
                    item.LoaiTaiKhoan1 = txtName.Text;
                    item.TrangThai = false;
                    if (chkStatus.Checked)
                        item.TrangThai = true;
                    var todo = _ltkBUS.LoaiTaiKhoan_Update(item);
                    if (todo)
                    {
                        this.Close();
                    }
                }
            }
        }
    }
}