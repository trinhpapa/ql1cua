﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI
{
    public partial class frmTypeLetterManager : Form
    {
        private readonly LoaiGiayToBUS _lgtBUS;
        private readonly TaiKhoanBUS _tkBUS;
        private readonly DonViBUS _dvBUS;

        public frmTypeLetterManager()
        {
            _lgtBUS = new LoaiGiayToBUS();
            _tkBUS = new TaiKhoanBUS();
            _dvBUS = new DonViBUS();
            InitializeComponent();
            dgvData_Binding();
            cbbUnit_Binding();
            cbbStatus_Binding();
            CheckRole();
        }

        private void CheckRole()
        {
            var data = _tkBUS.CheckRoleByUserId(Constants.UserInformation.Id);

            if (data.Contains(501))
            {
                btnAdd.Enabled = true;
            }
            if (data.Contains(502))
            {
                btnUpdate.Enabled = true;
            }
            if (data.Contains(503))
            {
                btnRemove.Enabled = true;
            }
        }

        private void cbbStatus_Binding()
        {
            var Item = new Dictionary<bool, string>();
            Item.Add(true, "Hoạt động");
            Item.Add(false, "Khóa");
            cbbStatus.DataSource = Item.ToList();
            cbbStatus.ValueMember = "Key";
            cbbStatus.DisplayMember = "Value";
        }

        private void cbbUnit_Binding()
        {
            cbbUnit.DataSource = _dvBUS.GetAll();
            cbbUnit.ValueMember = "Id";
            cbbUnit.DisplayMember = "TenDonVi";
        }

        private void dgvData_Binding()
        {
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = _lgtBUS.GetAlls();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var item = new LoaiGiayTo();
            item.LoaiGiayTo1 = txtName.Text.Trim();
            item.DonViTiepNhanId = (int)cbbUnit.SelectedValue;
            item.TrangThai = (bool)cbbStatus.SelectedValue;
            var todo = _lgtBUS.Create(item);
            if (todo)
            {
                MessageBox.Show("Thêm thành công", "Thông báo");
                dgvData_Binding();
            }
            else
            {
                MessageBox.Show("Thêm không thành công", "Thông báo");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var id = (int)dgvData.CurrentRow.Cells[0].Value;
            var item = new LoaiGiayTo();
            item.Id = id;
            item.LoaiGiayTo1 = txtName.Text.Trim();
            item.DonViTiepNhanId = (int)cbbUnit.SelectedValue;
            item.TrangThai = (bool)cbbStatus.SelectedValue;
            var todo = _lgtBUS.Update(item);
            if (todo)
            {
                MessageBox.Show("Cập nhật thành công", "Thông báo");
                dgvData_Binding();
            }
            else
            {
                MessageBox.Show("Cập nhật không thành công", "Thông báo");
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var id = (int)dgvData.CurrentRow.Cells[0].Value;
            _lgtBUS.Delete(id);
            dgvData_Binding();
        }

        private void dgvData_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtName.Text = dgvData.Rows[e.RowIndex].Cells[1].Value.ToString();
            cbbUnit.SelectedText = dgvData.Rows[e.RowIndex].Cells[2].Value.ToString();
            cbbStatus.SelectedValue = (bool)dgvData.Rows[e.RowIndex].Cells[3].Value;
        }
    }
}