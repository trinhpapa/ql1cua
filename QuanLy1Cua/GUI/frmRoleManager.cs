﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.GUI.FormAction;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI
{
    public partial class frmRoleManager : Form
    {
        private readonly LoaiTaiKhoanBUS _ltkBUS;
        private int typeAccountId { get; set; }

        public frmRoleManager()
        {
            InitializeComponent();
            _ltkBUS = new LoaiTaiKhoanBUS();
            dgvTypeAccount_Binding();
            dgvRole_Binding();
        }

        public void dgvTypeAccount_Binding()
        {
            dgvTypeAccount.AutoGenerateColumns = false;
            dgvTypeAccount.DataSource = _ltkBUS.LoaiTaiKhoan_GetAll();
        }

        public void dgvRole_Binding()
        {
            dgvRole.AutoGenerateColumns = false;
            dgvRole.DataSource = _ltkBUS.QuyenHeThong_GetAll();
        }

        private void dgvRole_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            SetCheckboxRoleByTypeAccount();
        }

        private void dgvTypeAccount_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            typeAccountId = (int)dgvTypeAccount.Rows[e.RowIndex].Cells[0].Value;
            SetCheckboxRoleByTypeAccount();
        }

        private void SetCheckboxRoleByTypeAccount()
        {
            var dataRole = _ltkBUS.PhanQuyen_GetAll((int)typeAccountId);
            foreach (DataGridViewRow row in dgvRole.Rows)
            {
                var itemChk = (DataGridViewCheckBoxCell)row.Cells[2];
                var id = (DataGridViewTextBoxCell)row.Cells[0];
                if (dataRole.Contains((int)id.Value))
                {
                    itemChk.Value = true;
                }
                else
                {
                    itemChk.Value = false;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmEditRole frm = new frmEditRole(null);
            frm.ShowDialog();
            dgvTypeAccount_Binding();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            frmEditRole frm = new frmEditRole((int)dgvTypeAccount.CurrentRow.Cells[0].Value);
            frm.ShowDialog();
            dgvTypeAccount_Binding();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Xác nhận xóa?", "Thông báo",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirmResult == DialogResult.Yes)
            {
                _ltkBUS.LoaiTaiKhoan_Remove((int)dgvTypeAccount.CurrentRow.Cells[0].Value);
                dgvTypeAccount_Binding();
            }
        }

        private void btnChangeRole_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Cập nhật quyền?", "Thông báo",
               MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (confirmResult == DialogResult.Yes)
            {
                List<int> listRole = new List<int>();
                foreach (DataGridViewRow row in dgvRole.Rows)
                {
                    var itemChk = (DataGridViewCheckBoxCell)row.Cells[2];
                    var id = (DataGridViewTextBoxCell)row.Cells[0];
                    if ((bool)itemChk.Value == true)
                    {
                        listRole.Add((int)id.Value);
                    }
                }
                var typeAccount = (int)dgvTypeAccount.CurrentRow.Cells[0].Value;
                var toto = _ltkBUS.PhanQuyen_Add(typeAccount, listRole);
                if (toto)
                {
                    MessageBox.Show("Cập nhật quyền thành công");
                    dgvRole_Binding();
                }
                else
                {
                    MessageBox.Show("Cập nhật quyền thất bại");
                }
            }
        }

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCheckAll.Checked == true)
            {
                foreach (DataGridViewRow row in dgvRole.Rows)
                {
                    var itemChk = (DataGridViewCheckBoxCell)row.Cells[2];
                    itemChk.Value = true;
                }
            }
            else
            {
                foreach (DataGridViewRow row in dgvRole.Rows)
                {
                    var itemChk = (DataGridViewCheckBoxCell)row.Cells[2];
                    itemChk.Value = false;
                }
            }
        }
    }
}