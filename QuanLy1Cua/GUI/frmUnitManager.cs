﻿using QuanLy1Cua.BUS;
using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLy1Cua.GUI
{
    public partial class frmUnitManager : Form
    {
        private readonly DonViBUS _dvBUS;
        private readonly TaiKhoanBUS _tkBUS;

        public frmUnitManager()
        {
            _dvBUS = new DonViBUS();
            _tkBUS = new TaiKhoanBUS();
            InitializeComponent();
            dgvData_Binding();
            cbbUnitStatus_Binding();
            CheckRole();
        }

        private void CheckRole()
        {
            var data = _tkBUS.CheckRoleByUserId(Constants.UserInformation.Id);

            if (data.Contains(401))
            {
                btnAdd.Enabled = true;
            }
            if (data.Contains(402))
            {
                btnUpdate.Enabled = true;
            }
            if (data.Contains(403))
            {
                btnRemove.Enabled = true;
            }
        }

        public void dgvData_Binding()
        {
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = _dvBUS.GetAlls();
        }

        public void cbbUnitStatus_Binding()
        {
            var Item = new Dictionary<bool, string>();
            Item.Add(true, "Hoạt động");
            Item.Add(false, "Khóa");
            cbbUnitStatus.DataSource = Item.ToList();
            cbbUnitStatus.ValueMember = "Key";
            cbbUnitStatus.DisplayMember = "Value";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var item = new DonVi();
            item.TenDonVi = txtUnitName.Text.Trim();
            item.DienThoai = txtUnitPhone.Text.Trim();
            item.TrangThai = (bool)cbbUnitStatus.SelectedValue;
            var todo = _dvBUS.Create(item);
            if (todo)
            {
                MessageBox.Show("Thêm thành công", "Thông báo");
                dgvData_Binding();
            }
            else
            {
                MessageBox.Show("Thêm không thành công", "Thông báo");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var id = (int)dgvData.CurrentRow.Cells[0].Value;
            var item = new DonVi();
            item.Id = id;
            item.TenDonVi = txtUnitName.Text.Trim();
            item.DienThoai = txtUnitPhone.Text.Trim();
            item.TrangThai = (bool)cbbUnitStatus.SelectedValue;
            var todo = _dvBUS.Update(item);
            if (todo)
            {
                MessageBox.Show("Cập nhật thành công", "Thông báo");
                dgvData_Binding();
            }
            else
            {
                MessageBox.Show("Cập nhật không thành công", "Thông báo");
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var id = (int)dgvData.CurrentRow.Cells[0].Value;
            _dvBUS.Delete(id);
            dgvData_Binding();
        }

        private void dgvData_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtUnitName.Text = dgvData.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtUnitPhone.Text = dgvData.Rows[e.RowIndex].Cells[2].Value.ToString();
            cbbUnitStatus.SelectedValue = (bool)dgvData.Rows[e.RowIndex].Cells[3].Value;
        }
    }
}