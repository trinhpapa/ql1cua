﻿using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DAO;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;

namespace QuanLy1Cua.BUS
{
    public class YeuCauXuLyBUS
    {
        private YeuCauXuLyDAO _ycxlDAO;
        private ChiTietYeuCauXuLyDAO _ctycxlDAO;

        public YeuCauXuLyBUS()
        {
            _ycxlDAO = new YeuCauXuLyDAO();
            _ctycxlDAO = new ChiTietYeuCauXuLyDAO();
        }

        public dynamic CheckStudentCodeExist(string studentCode)
        {
            return _ycxlDAO.GetByStudentCode(studentCode);
        }

        public dynamic GetById(long id)
        {
            return _ycxlDAO.GetById(id);
        }

        public bool ChangeStatusRemove(long id)
        {
            var item = _ctycxlDAO.Get(i => i.Id == id);
            if (item.TrangThaiYeuCauId == 1)
            {
                item.TrangThai = false;
                item.NguoiXoa = Constants.UserInformation.Id;
                _ctycxlDAO.Update(item);
                _ctycxlDAO.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public dynamic SearchByFullname(string fullname)
        {
            return _ycxlDAO.SearchByFullName(fullname);
        }

        public void ChangeStatus(long id, int status)
        {
            var item = _ctycxlDAO.Get(i => i.Id == id);
            item.TrangThaiYeuCauId = status;
            item.NguoiDoiTrangThaiYeuCau = Constants.UserInformation.Id;
            _ctycxlDAO.Update(item);
            _ctycxlDAO.Save();
        }

        public dynamic GetAllByDate(DateTime date, int status)
        {
            return _ycxlDAO.GetAllByDay(date, status);
        }

        public void Add(string HoTen, string MaSV, string DienThoai, long NguoiTiepNhan, List<ItemYCXL> letterItems)
        {
            var item = new YeuCauXuLy();
            item.HoTen = HoTen;
            item.MaSV = MaSV;
            item.DienThoai = DienThoai;
            item.NguoiTiepNhan = Constants.UserInformation.Id;
            item.NgayTiepNhan = DateTime.Now;
            _ycxlDAO.Add(item);
            _ycxlDAO.Save();

            foreach (var itemCT in letterItems)
            {
                var ct = new ChiTietYeuCauXuLy();
                ct.YeuCauXuLyId = item.Id;
                ct.LoaiGiayToId = itemCT.Id;
                ct.NgayHenTra = itemCT.NgayHen;
                ct.TrangThaiYeuCauId = 1;
                ct.TrangThai = true;

                _ctycxlDAO.Add(ct);
            }
            _ctycxlDAO.Save();
        }
    }
}