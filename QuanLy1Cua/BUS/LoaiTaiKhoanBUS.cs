﻿using QuanLy1Cua.DATA.DAO;
using QuanLy1Cua.DATA.DTO;
using System.Collections.Generic;
using System.Linq;

namespace QuanLy1Cua.BUS
{
    public class LoaiTaiKhoanBUS
    {
        private LoaiTaiKhoanDAO _ltkDAO;
        private QuyenHeThongDAO _qhtDAO;
        private PhanQuyenLoaiTaiKhoanDAO _pqDAO;

        public LoaiTaiKhoanBUS()
        {
            _ltkDAO = new LoaiTaiKhoanDAO();
            _qhtDAO = new QuyenHeThongDAO();
            _pqDAO = new PhanQuyenLoaiTaiKhoanDAO();
        }

        public bool LoaiTaiKhoan_Create(LoaiTaiKhoan model)
        {
            try
            {
                _ltkDAO.Add(model);
                _ltkDAO.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool LoaiTaiKhoan_Update(LoaiTaiKhoan model)
        {
            try
            {
                var item = _ltkDAO.Get(i => i.Id == model.Id);
                item.LoaiTaiKhoan1 = model.LoaiTaiKhoan1;
                item.TrangThai = model.TrangThai;
                _ltkDAO.Update(item);
                _ltkDAO.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void LoaiTaiKhoan_Remove(int Id)
        {
            _ltkDAO.Delete(i => i.Id == Id);
            _ltkDAO.Save();
        }

        public LoaiTaiKhoan LoaiTaiKhoan_GetById(int Id)
        {
            return _ltkDAO.Get(i => i.Id == Id);
        }

        public IEnumerable<LoaiTaiKhoan> LoaiTaiKhoan_GetAll()
        {
            return _ltkDAO.GetAll().ToList();
        }

        public IEnumerable<QuyenHeThong> QuyenHeThong_GetAll()
        {
            return _qhtDAO.GetAll().ToList();
        }

        public List<int?> PhanQuyen_GetAll(int LoaiTaiKhoanId)
        {
            return _pqDAO.GetRoleByTypeAccount(LoaiTaiKhoanId);
        }

        public bool PhanQuyen_Add(int typeAccount, List<int> listRole)
        {
            try
            {
                _pqDAO.Delete(i => i.LoaiTaiKhoanId == typeAccount);
                foreach (var itemRole in listRole)
                {
                    var item = new PhanQuyenLoaiTaiKhoan();
                    item.LoaiTaiKhoanId = typeAccount;
                    item.QuyenId = itemRole;
                    _pqDAO.Add(item);
                }
                _pqDAO.Save();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<LoaiTaiKhoan> GetAllByStatus(bool status)
        {
            return _ltkDAO.GetAll(i => i.TrangThai == status);
        }
    }
}