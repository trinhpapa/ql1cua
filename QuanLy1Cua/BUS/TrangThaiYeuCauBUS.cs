﻿using QuanLy1Cua.DATA.DAO;
using QuanLy1Cua.DATA.DTO;
using System.Collections.Generic;

namespace QuanLy1Cua.BUS
{
    public class TrangThaiYeuCauBUS
    {
        private TrangThaiYeuCauDAO _ttlDAO;

        public TrangThaiYeuCauBUS()
        {
            _ttlDAO = new TrangThaiYeuCauDAO();
        }

        public IEnumerable<TrangThaiYeuCau> GetAll()
        {
            return _ttlDAO.GetAll(i => i.TrangThai == true);
        }
    }
}