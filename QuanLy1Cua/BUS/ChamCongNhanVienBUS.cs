﻿using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DAO;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.BUS
{
    public class ChamCongNhanVienBUS
    {
        private ChamCongNhanVienDAO _dao;

        public ChamCongNhanVienBUS()
        {
            _dao = new ChamCongNhanVienDAO();
        }

        public void LoginTime(long userId, DateTime time)
        {
            var item = new ChamCongNhanVien();
            item.UserId = userId;
            item.LoginTime = time;
            _dao.Add(item);
            _dao.Save();
        }

        public ChamCongNhanVien GetLog(long userId, DateTime loginTime)
        {
            return _dao.Get(
                i => i.UserId == userId
                && i.LoginTime.Value.Year == loginTime.Year
                && i.LoginTime.Value.Month == loginTime.Month
                && i.LoginTime.Value.Day == loginTime.Day
                && i.LoginTime.Value.Hour == loginTime.Hour
                && i.LoginTime.Value.Minute == loginTime.Minute
                && i.LoginTime.Value.Second == loginTime.Second);
        }

        public void LogoutTime(long userId, DateTime loginTime, DateTime logoutTime)
        {
            var item = GetLog(userId, loginTime);
            if (item != null)
            {
                item.LogoutTime = logoutTime;
                _dao.Update(item);
                _dao.Save();
            }
        }

        public IEnumerable<ChamCongModel> GetListByMonth(DateTime time)
        {
            return _dao.GetListByMonth(time);
        }

        public IEnumerable<ChamCongModel> GetListByUser(string tenDangNhap, DateTime time)
        {
            return _dao.GetListByUser(tenDangNhap, time);
        }
    }
}