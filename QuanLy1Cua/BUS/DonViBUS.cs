﻿using QuanLy1Cua.DATA.DAO;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.BUS
{
    public class DonViBUS
    {
        private DonViDAO _dvDAO;

        public DonViBUS()
        {
            _dvDAO = new DonViDAO();
        }

        public List<DonVi> GetAll()
        {
            return _dvDAO.GetAll(i => i.TrangThai == true).ToList();
        }

        public List<DonVi> GetAlls()
        {
            return _dvDAO.GetAll().ToList();
        }

        public bool Create(DonVi model)
        {
            try
            {
                _dvDAO.Add(model);
                _dvDAO.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(DonVi model)
        {
            try
            {
                var item = _dvDAO.Get(i => i.Id == model.Id);
                item.TenDonVi = model.TenDonVi;
                item.DienThoai = model.DienThoai;
                item.TrangThai = model.TrangThai;
                _dvDAO.Update(item);
                _dvDAO.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Delete(int Id)
        {
            _dvDAO.Delete(i => i.Id == Id);
            _dvDAO.Save();
        }
    }
}