﻿using QuanLy1Cua.DATA.DAO;
using QuanLy1Cua.DATA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLy1Cua.BUS
{
    public class LoaiGiayToBUS
    {
        private LoaiGiayToDAO _lgtDAO;

        public LoaiGiayToBUS()
        {
            _lgtDAO = new LoaiGiayToDAO();
        }

        public List<LoaiGiayTo> GetAll()
        {
            return _lgtDAO.GetAll(i => i.TrangThai == true).ToList();
        }

        public dynamic GetAlls()
        {
            return _lgtDAO.GetAlls();
        }

        public LoaiGiayTo GetInfo(int id)
        {
            return _lgtDAO.Get(i => i.Id == id);
        }

        public bool Create(LoaiGiayTo model)
        {
            try
            {
                _lgtDAO.Add(model);
                _lgtDAO.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(LoaiGiayTo model)
        {
            try
            {
                var item = _lgtDAO.Get(i => i.Id == model.Id);
                item.LoaiGiayTo1 = model.LoaiGiayTo1;
                item.DonViTiepNhanId = model.DonViTiepNhanId;
                item.TrangThai = model.TrangThai;
                _lgtDAO.Update(item);
                _lgtDAO.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Delete(int Id)
        {
            _lgtDAO.Delete(i => i.Id == Id);
            _lgtDAO.Save();
        }
    }
}