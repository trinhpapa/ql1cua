﻿using QuanLy1Cua.Core;
using QuanLy1Cua.DATA.DAO;
using QuanLy1Cua.DATA.DTO;
using System.Collections.Generic;

namespace QuanLy1Cua.BUS
{
    public class TaiKhoanBUS
    {
        private readonly TaiKhoanDAO _tkDAO;
        private readonly QuyenHeThongDAO _htDAO;
        private readonly PhanQuyenLoaiTaiKhoanDAO _pqDAO;

        public TaiKhoanBUS()
        {
            _tkDAO = new TaiKhoanDAO();
            _htDAO = new QuyenHeThongDAO();
            _pqDAO = new PhanQuyenLoaiTaiKhoanDAO();
        }

        public TaiKhoan GetAccountByCredentials(string username, string password)
        {
            return _tkDAO.Get(i => i.TenDangNhap == username && i.MatKhauMD5 == password && i.TrangThai == true);
        }

        public dynamic GetAll()
        {
            return _tkDAO.GetAll();
        }

        public TaiKhoan GetById(long Id)
        {
            return _tkDAO.Get(i => i.Id == Id);
        }

        public string CreateAccount(TaiKhoan model)
        {
            try
            {
                var accountExist = _tkDAO.Get(i => i.TenDangNhap == model.TenDangNhap);
                if (accountExist == null)
                {
                    _tkDAO.Add(model);
                    _tkDAO.Save();
                    return model.TenDangNhap;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public string UpdateAccount(TaiKhoan model)
        {
            try
            {
                var accountExist = _tkDAO.Get(i => i.TenDangNhap == model.TenDangNhap && i.Id != model.Id);
                if (accountExist == null)
                {
                    var item = _tkDAO.Get(i => i.Id == model.Id);
                    item.TenDangNhap = model.TenDangNhap;
                    item.HoTen = model.HoTen;
                    if (model.MatKhauMD5 != null)
                        item.MatKhauMD5 = StringHelper.ConverToMD5Hash(model.MatKhauMD5);
                    item.TrangThai = model.TrangThai;
                    item.LoaiTaiKhoanId = model.LoaiTaiKhoanId;
                    _tkDAO.Update(item);
                    _tkDAO.Save();
                    return model.TenDangNhap;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public bool RemoveAcount(long Id)
        {
            try
            {
                _tkDAO.Delete(i => i.Id == Id);
                _tkDAO.Save();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<int?> CheckRoleByUserId(long Id)
        {
            var typeAccount = _tkDAO.Get(i => i.Id == Id).LoaiTaiKhoanId;
            return _pqDAO.GetRoleByTypeAccount(typeAccount);
        }
    }
}